import os, sys, filecmp
from conan import ConanFile
from conan.tools.build import cross_building
from conan.tools.layout import basic_layout

class CalculatorTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "VirtualRunEnv"
    apply_env = False
    test_type = "explicit"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def layout(self):
        basic_layout(self)

    def test(self):
        if cross_building(self):
            return

        self.run("scientific_calculator help", env="conanrun")
        self.run("scientific_calculator 2+2", env="conanrun")
        self.run("scientific_calculator 2+2*3", env="conanrun")
        self.run("scientific_calculator \(2+2\)*3", env="conanrun")
        self.run("scientific_calculator x+5=4", env="conanrun")
        self.run("scientific_calculator 2*x-1=3", env="conanrun")
        self.run("scientific_calculator cos\(0\)", env="conanrun")
        self.run("scientific_calculator cot\(0\)", env="conanrun")
        self.run("scientific_calculator log\(pi\)", env="conanrun")
        
