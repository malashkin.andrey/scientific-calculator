#include <cassert>
#include <cmath>
#include <iostream>
#include <stack>
#include <string_view>

#include "scientificcalculator.h"
#include "tokens/token.h"
#include "tokens/parenthesis.h"
#include "tokens/space.h"
#include "tokens/number.h"
#include "tokens/basicmathoperations.h"
#include "tokens/trigonometricoperations.h"
#include "tokens/constants.h"
#include "tokens/logoperation.h"
#include "tokens/equalssign.h"

std::shared_ptr<Token> ScientificCalculator::nextToken(const std::string &input, int &position)
{
    assert(position < input.size());
    const char& currentChar = input[position];

    std::shared_ptr<Token> result;

    if(currentChar == '+') // TODO this condition can be called from a related Token.
        result = std::make_shared<PlusOperation>();
    else if(currentChar == '-')
        result = std::make_shared<MinusOperation>();
    else if(currentChar == '*')
        result = std::make_shared<MultiplicationOperation>();
    else if(currentChar == '/')
        result = std::make_shared<DivisionOperation>();
    else if(isdigit(currentChar))
        result = std::make_shared<Number>();
    else if(currentChar == ' ')
        result = Space::getInstance();
    else if(currentChar == '(')
        result = std::make_shared<OpenParenthesis>();
    else if(currentChar == ')')
        result = std::make_shared<CloseParenthesis>();
    else if(currentChar == 'p' || currentChar == 'P')
        result = std::make_shared<PiConstant>();
    else if(currentChar == 'e' || currentChar == 'E')
        result = std::make_shared<EConstant>();
    else if(currentChar == 'x' || currentChar == 'X' || currentChar == 'y' || currentChar == 'Y')
        result = std::make_shared<Variable>();
    else if(currentChar == '=')
        result = std::make_shared<EqualsSign>();
    if(!result && position + 2 < input.size())
    {
        std::string_view twoLetters(input.begin() + position, input.begin() + position + 2);
        if(twoLetters == "ln")
            result = std::make_shared<LnOperation>();
    }
    if(!result && (position + 3 < input.size()))
    {
        std::string_view threeLetters(input.begin() + position, input.begin() + position + 3);
        if(threeLetters == "cos")
            result = std::make_shared<CosOperation>();
        else if(threeLetters == "sin")
            result = std::make_shared<SinOperation>();
        else if(threeLetters == "tan")
            result = std::make_shared<TanOperation>();
        else if(threeLetters == "cot")
            result = std::make_shared<CtanOperation>();

        else if(threeLetters == "Log" || threeLetters == "log")
            result = std::make_shared<LogOperation>();
    }

    if(!result)
        throw std::runtime_error(std::string("failed to parse token at position ") + std::to_string(position));

    result->consumeTokenLength(input, position);
    return result;
}

std::shared_ptr<Token> ScientificCalculator::getSolutionToken(const std::string &input)
{
    std::cout << "\nInput: " << input << std::endl;

    std::list<std::shared_ptr<Token>> tokens;
    parseToTokens(input, tokens);
    std::cout << "\nTokenized input:\n";
    printTokens(tokens);

    removeSpaces(tokens);
    std::cout << "\nWithout spaces:\n";
    printTokens(tokens);

    if(isEquation(tokens))
    {
        equationToNormalForm(tokens);
        std::cout << "\nEquation in normal form:\n";
        printTokens(tokens);
    }

    if(isPolishNotated(tokens))
    {
        std::cout << "\nalready in polish notation" << std::endl;
    }
    else
    {
        std::list<std::shared_ptr<Token>> polishNotated;
        bool inserted = insertMissedMultiplySigns(tokens);
        if(inserted)
        {
            std::cout << "\nInserted missed multiplication signs:";
            printTokens(tokens);
        }

        toPolishNotation(tokens, polishNotated);
        std::cout << "\nPolish notated:\n";
        printTokens(polishNotated, true);
        tokens = polishNotated;
    }

    return getSolutionToken(tokens);
}

void ScientificCalculator::printTokens(const std::list<std::shared_ptr<Token> > &tokens, bool insertSpaces)
{
    for(const auto& token : tokens)
    {
        token->print();
        if(insertSpaces)
            std::cout << " ";
    }
    std::cout.flush();
}

bool ScientificCalculator::isEquation(const std::list<std::shared_ptr<Token> > &tokens)
{
    int equalsSignCount = 0;
    for(auto iter = tokens.begin(); iter != tokens.end(); ++iter)
    {
        if(dynamic_cast<EqualsSign*>(iter->get()))
            ++equalsSignCount;
    }
    if(equalsSignCount > 1)
        throw std::runtime_error(std::string("failed to analyze an equation: too many equal signs"));
    return equalsSignCount;
}

void ScientificCalculator::equationToNormalForm(std::list<std::shared_ptr<Token>>& tokens)
{
    bool containsEqualsSign = false;
    std::list<std::shared_ptr<Token>>::iterator equalsSignPosition;

    for(auto iter = tokens.begin(); iter != tokens.end(); ++iter)
    {
        if(dynamic_cast<EqualsSign*>(iter->get()))
        {
            equalsSignPosition = iter;
            containsEqualsSign = true;
            break;
        }
    }
    assert(containsEqualsSign);

    // substitute - ( instead of =
    *equalsSignPosition = std::make_shared<MinusOperation>();
    ++equalsSignPosition;
    tokens.insert(equalsSignPosition, std::make_shared<OpenParenthesis>());
    tokens.push_back(std::make_shared<CloseParenthesis>());
}

double ScientificCalculator::solveNormalizedEquation(const std::shared_ptr<Token> &token)
{
    if(token->variableValue().has_value())
        return (token->value() * -1) / token->variableValue().value();
    return token->value();
}

void ScientificCalculator::parseToTokens(const std::string &input, std::list<std::shared_ptr<Token> > &output)
{
    int i = 0;
    while (i < input.size())
    {
        auto token = nextToken(input, i);
        output.push_back(token);
    }
}

bool ScientificCalculator::insertMissedMultiplySigns(std::list<std::shared_ptr<Token> >& tokens)
{
    bool inserted = false;
    bool previousIsNumber = false;
    for(auto iter = tokens.begin(); iter != tokens.end(); ++iter)
    {
        bool isNumber = dynamic_cast<Number*>(iter->get());
        if(previousIsNumber && isNumber)
        {
            iter = tokens.insert(iter, std::make_shared<MultiplicationOperation>());
            isNumber = false;
            inserted = true;
        }
        previousIsNumber = isNumber;
    }
    return inserted;
}

void ScientificCalculator::removeSpaces(std::list<std::shared_ptr<Token> > &tokens)
{
    tokens.erase(std::remove_if(tokens.begin(), tokens.end(),
                                [](const std::shared_ptr<Token>& t)
    {
        return t == Space::getInstance();
    }),
                 tokens.end());
}

void ScientificCalculator::toPolishNotation(const std::list<std::shared_ptr<Token> > &tokens, std::list<std::shared_ptr<Token> > &output)
{
    std::stack<std::shared_ptr<Token>> operations;
    for(const auto& token : tokens)
    {
        if(token->isParenthesis())
        {
            if(dynamic_cast<OpenParenthesis*>(token.get()))
                operations.push(token);
            else if(dynamic_cast<CloseParenthesis*>(token.get()))
            {
                while(operations.size() && !dynamic_cast<OpenParenthesis*>(operations.top().get()))
                {
                    output.push_back(operations.top());
                    operations.pop();
                }
                assert(operations.size());
                operations.pop();
            }
            else
                assert(false);
        }
        else if(token->isOperation())
        {
            while(operations.size() && operations.top()->priority() > token->priority())
            {
                output.push_back(operations.top());
                operations.pop();
            }
            operations.push(token);
        }
        else
            output.push_back(token);
    }

    while(operations.size())
    {
        output.push_back(operations.top());
        operations.pop();
    }
}

bool ScientificCalculator::isPolishNotated(const std::list<std::shared_ptr<Token> > &tokens)
{
    int operandsBalance = 0;
    for(const auto& token : tokens)
    {
        if(token->isParenthesis())
            return false;
        if(token->operandsCount() == 0)
            ++operandsBalance;
        else if(token->operandsCount() == 2)
            --operandsBalance;

        if(operandsBalance < 1)
            return false;
    }
    // If operandsBalance != 1 here, there will be an error in evaluation
    return true;
}

std::shared_ptr<Token> ScientificCalculator::getSolutionToken(const std::list<std::shared_ptr<Token> > &polishNotatedTokens)
{
    std::stack<std::shared_ptr<Token>> stack;
    for(const auto& token : polishNotatedTokens)
    {
        if(token->isOperation())
        {
            if(stack.size() < token->operandsCount())
                throw std::runtime_error(std::string("failed to calculate: there are more operations rather than the operands"));
            assert(token->operandsCount() > 0 && (token->operandsCount() <= 2));

            if(token->operandsCount() == 1)
            {
                auto token1 = stack.top();
                stack.pop();
                std::shared_ptr<Token> dummy;
                auto result = token->calculate(token1, dummy);
                stack.push(result);
            }
            if(token->operandsCount() == 2)
            {
                auto token2 = stack.top();
                stack.pop();
                auto token1 = stack.top();
                stack.pop();
                auto result = token->calculate(token1, token2);
                stack.push(result);
            }
        }
        else
        {
            stack.push(token);
        }
    }

    if(stack.size() != 1)
        throw std::runtime_error(std::string("failed to calculate: there are more operands rather than the operations"));

    assert(stack.top()->isOperation() == false);

    return stack.top();
}

void ScientificCalculator::printSolution(const std::string &input)
{
    try {
        auto solution = getSolutionToken(input);
        double result = solveNormalizedEquation(solution);
        if(solution->variableValue().has_value())
            std::cout << "\nResult: x = " << result << std::endl;
        else
            std::cout << "\nResult: " << result << std::endl;

        if(!std::isfinite(result))
            throw std::runtime_error(std::string("failed to calculate: result is not a number"));
    }
    catch( const std::exception & ex )
    {
        std::cerr << "\nFailed to calculate: ";
        std::cerr << ex.what() << std::endl;
    }
}

double ScientificCalculator::calculate(const std::string &input)
{
    auto solution = getSolutionToken(input);
    double result = solveNormalizedEquation(solution);
    if(solution->variableValue().has_value())
        std::cout << "\nResult: x = " << result << std::endl;
    else
        std::cout << "\nResult: " << result << std::endl;

    return result;
}
