#ifndef SCIENTIFICCALCULATOR_H
#define SCIENTIFICCALCULATOR_H

#include <memory>
#include <list>

class Token;

class ScientificCalculator
{
public:
    static void printSolution(const std::string& input);
    static double calculate(const std::string& input);

private:
    static std::shared_ptr<Token> nextToken(const std::string& input, int& position);
    static std::shared_ptr<Token> getSolutionToken(const std::string& input);
    static std::shared_ptr<Token> getSolutionToken(const std::list<std::shared_ptr<Token>>& polishNotatedTokens);
    static bool isPolishNotated(const std::list<std::shared_ptr<Token>>& tokens);
    static void toPolishNotation(const std::list<std::shared_ptr<Token>>& tokens, std::list<std::shared_ptr<Token>>& output);
    static void removeSpaces(std::list<std::shared_ptr<Token>>& tokens);
    static void parseToTokens(const std::string& input, std::list<std::shared_ptr<Token>>& output);
    static bool insertMissedMultiplySigns(std::list<std::shared_ptr<Token>>& tokens);
    static void printTokens(const std::list<std::shared_ptr<Token>>& tokens, bool insertSpaces = false);

    static bool isEquation(const std::list<std::shared_ptr<Token>>& tokens);
    static void equationToNormalForm(std::list<std::shared_ptr<Token> > &tokens);
    static double solveNormalizedEquation(const std::shared_ptr<Token>& token);

    ScientificCalculator() = delete;
};

#endif // SCIENTIFICCALCULATOR_H
