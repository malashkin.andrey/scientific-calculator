#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "number.h"

class Constant : public Number
{
public:
    virtual void consumeTokenLength(const std::string& input, int& position) override = 0;
    virtual void print() const override = 0;
};

class PiConstant : public Constant
{
public:
    virtual void consumeTokenLength(const std::string& input, int& position) override;
    virtual void print() const override;
};

class EConstant : public Constant
{
public:
    virtual void consumeTokenLength(const std::string& input, int& position) override;
    virtual void print() const override;
};

#endif // CONSTANTS_H
