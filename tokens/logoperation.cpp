#include <cassert>
#include <bits/stdc++.h>
#include "logoperation.h"
#include "number.h"

bool LogOperation::isOperation()
{
    return true;
}

int LogOperation::operandsCount()
{
    return 1;
}

void LogOperation::consumeTokenLength(const std::string& input, int &position)
{
    assert(input.size() > position + 3);
    position += 3;

    while(input[position] == ' ')
        ++position;

    if(input[position] != '(')
        m_base = Number::acquireNumber(input, position);
}

std::shared_ptr<Token> LogOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    arg1->setValue(log2(arg1->value()) / log2(m_base));
    return arg1;
}

void LogOperation::print() const
{
  std::cout << "log" << m_base;
}

int LogOperation::priority()
{
    return 5;
}

LnOperation::LnOperation()
{
    m_base = M_E;
}

int LnOperation::operandsCount()
{
    return 1;
}

void LnOperation::consumeTokenLength(const std::string &input, int &position)
{
    assert(input.size() > position + 2);
    position += 2;
}
