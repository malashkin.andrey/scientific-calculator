#include <cassert>
#include <iostream>
#include "space.h"


Space::Space() {}

std::shared_ptr<Space> &Space::getInstance()
{
    static std::shared_ptr<Space> instance(new Space());
    return instance;
}

bool Space::isOperation()
{
    return false;
}

std::shared_ptr<Token> Space::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    assert(false);
}

void Space::consumeTokenLength(const std::string &, int &position)
{
    ++position;
}

void Space::print() const
{
    std::cout << " ";
}

int Space::priority()
{
    return 0; // Does not make sense and shouldn't be called
}

int Space::operandsCount()
{
    return 0;
}
