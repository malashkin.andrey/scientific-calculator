#ifndef TOKEN_H
#define TOKEN_H

#include <memory>
#include <optional>
#include <string>
#include <cassert>

class Token
{
public:
    Token() = default;
    virtual ~Token() = default;
    virtual bool isOperation() = 0;
    virtual bool isParenthesis() { return false; }
    virtual std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
    {
        assert(false);
        return std::shared_ptr<Token> {};
    }

    //! \brief value returns constant. Does not make sense for all operations.
    virtual double value() { assert(false); return -1; }
    //! \brief Returns multiplier of variable part (x or y).
    virtual std::optional<double> variableValue() { return std::nullopt; }

    virtual void setValue(double newValue) { assert(false); };
    virtual void setVariableValue(double newVariableValue) { assert(false); }

    //!
    //! \brief consumeTokenLength Helper method for Token input. It may vary for example for numbers, while for math operators it's constant.
    //! \param input input string
    //! \param position input position
    //!
    virtual void consumeTokenLength(const std::string& input, int& position) = 0;
    virtual void print() const = 0;
    virtual int priority() = 0;
    virtual int operandsCount() = 0;

    static char previousChar(const std::string& input, int position);
};

inline char Token::previousChar(const std::string &input, int position)
{
    --position;
    while(position >= 0)
    {
        if(input[position] != ' ')
            return input[position];
        --position;
    }
    return '\0';
}

#endif // TOKEN_H
