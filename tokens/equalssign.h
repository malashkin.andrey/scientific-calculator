#ifndef EQUALSSIGN_H
#define EQUALSSIGN_H

#include "token.h"

class EqualsSign : public Token
{
public:
    EqualsSign() = default;
    bool isOperation();

    void consumeTokenLength(const std::string &, int &position);
    void print() const;
    int priority();
    int operandsCount();
};

#endif // EQUALSSIGN_H
