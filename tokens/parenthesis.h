#ifndef PARENTHESIS_H
#define PARENTHESIS_H

#include "token.h"

class Parenthesis : public Token
{
public:
    virtual bool isOperation() override;
    virtual void consumeTokenLength(const std::string&, int& position) override;
    virtual int operandsCount() override;
    virtual bool isParenthesis() override;
};

class OpenParenthesis : public Parenthesis
{
public:
    virtual void print() const override;
    virtual int priority() override;
};

class CloseParenthesis : public Parenthesis
{
public:
    virtual void print() const override;
    virtual int priority() override;
};


#endif // PARENTHESIS_H
