#ifndef NUMBER_H
#define NUMBER_H

#include "token.h"

class Number : public Token
{
public:
    Number() = default;
    Number(double value);

    virtual bool isOperation() override;
    static double acquireNumber(const std::string& input, int& position);
    virtual void consumeTokenLength(const std::string& input, int& position) override;
    virtual void print() const override;
    virtual int priority() override;
    virtual int operandsCount() override;

    virtual double value() override;
    virtual void setValue(double newValue) override;

    virtual std::optional<double> variableValue() override;
    virtual void setVariableValue(double newVariableValue) override;

protected:
    double m_value = 0;
    std::optional<double> m_variableMultiplier;
};

class Variable : public Number
{
public:
    virtual void consumeTokenLength(const std::string& input, int& position) override;
    virtual void print() const override;

private:
    char m_variableName;
};

#endif // NUMBER_H
