#include <math.h>
#include <iostream>
#include "constants.h"

void PiConstant::consumeTokenLength(const std::string &input, int &position)
{
    position += 2;
    m_value = M_PI;
}

void PiConstant::print() const
{
    std::cout << "pi";
}

void EConstant::consumeTokenLength(const std::string &input, int &position)
{
    ++position;
    m_value = M_E;
}

void EConstant::print() const
{
    std::cout << "e";
}
