#include <iostream>
#include <cassert>

#include "basicmathoperations.h"

bool BasicMathOperation::isOperation()
{
    return true;
}

void BasicMathOperation::consumeTokenLength(const std::string& input, int &position)
{
    ++position;
}

int MinusOperation::operandsCount()
{
    return m_isUnaryOperator ? 1 : 2;
}

std::shared_ptr<Token> MinusOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    if(m_isUnaryOperator)
    {
        assert(!arg2);
        arg1->setValue(arg1->value() * -1);
        if(arg1->variableValue().has_value())
            arg1->setVariableValue(arg1->variableValue().value() * -1);

    }
    else
    {
        arg1->setValue(arg1->value() - arg2->value());
        if(arg1->variableValue().has_value() || arg2->variableValue().has_value())
            arg1->setVariableValue(arg1->variableValue().value_or(0) - arg2->variableValue().value_or(0));

    }
    return arg1;
}

void MinusOperation::consumeTokenLength(const std::string &input, int &position)
{
    char previous = previousChar(input, position);
    if(previous == '\0' || previous == '(')
        m_isUnaryOperator = true;
    ++position;
}

void MinusOperation::print() const
{
    std::cout << "-";
}

int MinusOperation::priority()
{
    return m_isUnaryOperator? 5 : 1;
}

std::shared_ptr<Token> PlusOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    arg1->setValue(arg1->value() + arg2->value());
    if(arg1->variableValue().has_value() || arg2->variableValue().has_value())
        arg1->setVariableValue(arg1->variableValue().value_or(0) + arg2->variableValue().value_or(0));
    return arg1;
}

void PlusOperation::print() const
{
    std::cout << "+";
}

int PlusOperation::priority()
{
    return 1;
}

std::shared_ptr<Token> MultiplicationOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    if(arg1->variableValue().has_value() || arg2->variableValue().has_value())
    {
        if(arg1->variableValue().has_value() && arg2->variableValue().has_value())
            throw std::runtime_error(std::string("Calculator can only solve simple equation, no quadratic equation allowed"));

        arg1->setVariableValue(arg1->variableValue().value_or(0) * arg2->value() + arg1->value() * arg2->variableValue().value_or(0));
    }

    arg1->setValue(arg1->value() * arg2->value());
    return arg1;
}

void MultiplicationOperation::print() const
{
    std::cout << "*";
}

int MultiplicationOperation::priority()
{
    return 2;
}

std::shared_ptr<Token> DivisionOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2)
{
    if(arg2->value() == 0)
        throw std::runtime_error(std::string("Division by 0"));

    arg1->setValue(arg1->value() / arg2->value());

    if(arg1->variableValue().has_value())
    {
        if(arg2->variableValue().has_value())
            throw std::runtime_error(std::string("Division by variable is not yet implemented"));

        arg1->setVariableValue(arg1->variableValue().value() / arg2->value());
    }

    return arg1;
}

void DivisionOperation::print() const
{
    std::cout << "/";
}

int DivisionOperation::priority()
{
    return 2;
}
