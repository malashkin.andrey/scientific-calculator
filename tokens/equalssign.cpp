#include <cassert>
#include <iostream>

#include "equalssign.h"

bool EqualsSign::isOperation()
{
    return true;
}

void EqualsSign::consumeTokenLength(const std::string &, int &position)
{
    ++position;
}

void EqualsSign::print() const
{
    std::cout << "=";
}

int EqualsSign::priority()
{
    assert(false);
    return 0;
}

int EqualsSign::operandsCount()
{
    assert(false);
    return 0;
}
