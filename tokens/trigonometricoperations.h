#ifndef TRIGONOMETRICOPERATIONS_H
#define TRIGONOMETRICOPERATIONS_H

#include "token.h"

class TrigonometricOperation : public Token
{
public:
    virtual bool isOperation() override;
    virtual int operandsCount() override;
    virtual void consumeTokenLength(const std::string&, int& position) override;
    virtual int priority() override;
};

class CosOperation : public TrigonometricOperation
{
public:
    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& );
    void print() const;
};

class SinOperation : public TrigonometricOperation
{
public:
    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& );
    void print() const;
};

class TanOperation : public TrigonometricOperation
{
public:
    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& );
    void print() const;
};

class CtanOperation : public TrigonometricOperation
{
public:
    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& );
    void print() const;
};

#endif // TRIGONOMETRICOPERATIONS_H
