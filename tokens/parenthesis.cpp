#include <iostream>
#include <cassert>
#include "parenthesis.h"

bool Parenthesis::isOperation()
{
    return true;
}

void Parenthesis::consumeTokenLength(const std::string &, int &position)
{
    ++position;
}

int Parenthesis::operandsCount()
{
    return 0;
}

bool Parenthesis::isParenthesis()
{
    return true;
}

void OpenParenthesis::print() const
{
    std::cout << "(";
}

int OpenParenthesis::priority()
{
    return INT_MIN;
}

void CloseParenthesis::print() const
{
    std::cout << ")";
}

int CloseParenthesis::priority()
{
    return INT_MAX;
}
