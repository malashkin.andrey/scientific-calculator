#ifndef BASICMATHOPERATION_H
#define BASICMATHOPERATION_H

#include "token.h"

class BasicMathOperation : public Token
{
public:
    virtual bool isOperation() override;
    virtual int operandsCount() override { return 2; }
    virtual void consumeTokenLength(const std::string& input, int& position) override;
};

class MinusOperation : public BasicMathOperation
{
public:
    virtual int operandsCount() override;
    virtual std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2) override;
    virtual void consumeTokenLength(const std::string& input, int& position) override;

    virtual void print() const override;
    virtual int priority() override;
private:
    bool m_isUnaryOperator = false;
};

class PlusOperation : public BasicMathOperation
{
public:
    virtual std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2) override;
    virtual void print() const override;
    virtual int priority() override;
};

class MultiplicationOperation : public BasicMathOperation
{
public:
    virtual std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2) override;
    virtual void print() const override;
    virtual int priority() override;
};

class DivisionOperation : public BasicMathOperation
{
public:
    virtual std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2) override;
    virtual void print() const override;
    virtual int priority() override;
};
#endif // BASICMATHOPERATION_H
