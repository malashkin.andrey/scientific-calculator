#include <iostream>
#include <cassert>
#include "number.h"

Number::Number(double value) :
    m_value(value)
{}

bool Number::isOperation()
{
    return false;
}

double Number::acquireNumber(const std::string &input, int &position)
{
    double result = 0;
    bool beforeDot = true;
    for(; position <= input.size(); ++position)
    {
        double fractionalPartMultiplier = 1;

        if(std::isdigit(input[position]))
        {
            int digit = input[position] - '0';
            if(beforeDot)
                result = result * 10 + digit;
            else
            {
                fractionalPartMultiplier /= 10;
                result += digit * fractionalPartMultiplier;
            }
        }
        else if(input[position] == '.')
            beforeDot = false;
        else
            break;
    }
    return result;
}

void Number::consumeTokenLength(const std::string &input, int &position)
{
    m_value = acquireNumber(input, position);
}

void Number::print() const
{
    std::cout << m_value;
}

int Number::priority()
{
    return 0; // Does not make sense and shouldn't be called
}

int Number::operandsCount()
{
    return 0;
}

double Number::value()
{
    return m_value;
}

void Number::setValue(double newValue)
{
    m_value = newValue;
}

void Variable::consumeTokenLength(const std::string &input, int &position)
{
    m_variableName = input[position];
    m_variableMultiplier = 1;
    ++position;
}

void Variable::print() const
{
    std::cout << m_variableName;
}

std::optional<double> Number::variableValue()
{
    return m_variableMultiplier;
}

void Number::setVariableValue(double newVariableValue)
{
    m_variableMultiplier = newVariableValue;
}
