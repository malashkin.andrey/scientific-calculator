#include <cmath>
#include <iostream>
#include <complex.h>
#include "trigonometricoperations.h"

bool TrigonometricOperation::isOperation()
{
    return true;
}

int TrigonometricOperation::operandsCount()
{
    return 1;
}

void TrigonometricOperation::consumeTokenLength(const std::string &, int &position)
{
    position += 3;
}

int TrigonometricOperation::priority()
{
    return 3;
}

std::shared_ptr<Token> CosOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& )
{
    arg1->setValue(std::cos(arg1->value()));
    return arg1;
}

void CosOperation::print() const
{
    std::cout << "cos";
}


std::shared_ptr<Token> SinOperation::calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>&)
{
    arg1->setValue(std::sin(arg1->value()));
    return arg1;
}

void SinOperation::print() const
{
    std::cout << "sin";
}

std::shared_ptr<Token> TanOperation::calculate(std::shared_ptr<Token> &arg1, std::shared_ptr<Token> &)
{
    arg1->setValue(std::tan(arg1->value()));
    return arg1;
}

void TanOperation::print() const
{
    std::cout << "tan";
}

std::shared_ptr<Token> CtanOperation::calculate(std::shared_ptr<Token> &arg1, std::shared_ptr<Token> &)
{
    // that evaluation saves the most precision
    arg1->setValue(std::tan(M_PI_2 - arg1->value()));
    return arg1;
}

void CtanOperation::print() const
{
    std::cout << "ctan";
}
