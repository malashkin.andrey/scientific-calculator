#ifndef LOGOPERATION_H
#define LOGOPERATION_H

#include "token.h"

class LogOperation : public Token
{
public:
    LogOperation() = default;
    virtual bool isOperation() override;
    virtual int operandsCount() override;
    virtual void consumeTokenLength(const std::string& input, int& position) override;

    //! \brief calculate takes only one argument. Log base is already parsed via 'consumeTokenLength'. If not specified, base = 10
    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>&) override;
    void print() const override;
    int priority() override;

protected:
    double m_base = 10;
};

class LnOperation : public LogOperation
{
public:
    LnOperation();
    virtual int operandsCount() override;
    virtual void consumeTokenLength(const std::string& input, int& position) override;
};

#endif // LOGOPERATION_H
