#ifndef SPACE_H
#define SPACE_H

#include "token.h"

class Space : public Token
{
private:
    Space();
    Space(Space const&) = delete;
    void operator=(Space const&) = delete;

public:
    static std::shared_ptr<Space>& getInstance();

    virtual bool isOperation() override;

    std::shared_ptr<Token> calculate(std::shared_ptr<Token>& arg1, std::shared_ptr<Token>& arg2) override;

    virtual void consumeTokenLength(const std::string&, int& position) override;
    virtual void print() const override;
    virtual int priority() override;
    virtual int operandsCount() override;
};

#endif // SPACE_H
