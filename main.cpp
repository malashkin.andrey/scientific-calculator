#include <assert.h>
#include <cmath>
#include <iostream>
#include <string.h>
#include <limits>

#include "scientificcalculator.h"

const double EPSILON = 0.00000001;

void test(const std::string& expression, double expectedResult)
{
    double result = ScientificCalculator::calculate(expression);

    if(std::isfinite(result))
    {
        double delta = std::abs(expectedResult - result);
        if(delta > EPSILON)
        {
            std::cerr << "expression calculated wrong: " << expression << " expected result " << expectedResult << " got " << result << std::endl;
            assert(false);
        }
    }
    else
        assert(result == expectedResult);
}

void runTests()
{
    test("0", 0);
    test("5", 5);
    test("1.1", 1.1);

    test("0-1", -1);
    test("1 2 3 * +", 7);
    test("5 2 + 3 *", 21);
    test("5 2 * 2 * 4 +", 24);
    test("5 2 + 2 + 4 +", 13);
    test("3 2 + 2 4 + *", 30);
    test("3 2 2 4 + + *", 24);
    test("0 1 *", 0);
    test("5 5 + 6 6 + 1 1 + * +", 34);
    test("1156 12 11 * +", 1288);

    test("1 + 2 * 3", 7);
    test("1 * 2 + 3", 5);
    test("11 * 22", 242);

    // spaces test:
    test(" 1000 ", 1000);
    test("1 + 1", 2);
    test("2 - 3", -1);
    test("5* 2", 10);
    test("4/2", 2);

    // parenthesis test:
    test("(1+2)*3", 9);
    test("((1))", 1);

    // trigonometry test:
    test("cos(0)", 1);
    test("sin(0)", 0);
    test("tan(0)", 0);

    test("cos(1)", 0.54030230586);
    test("sin(1)", 0.8414709848);
    test("tan(1)", 1.55740772465);
    test("cot(1)", 0.642092616);

    // unary minus tests:
    test("-1", -1);
    test(" -7 ", -7);
    test("(-11 + 11)", 0);
    test("(-5)", -5);

    // logarithm test:
    test("log(1)", 0);
    test("log(e)", 0.4342944819);
    test("log2(e)", 1.4426950408);
    test("log2(e+pi)", 2.5508697625);

    test("ln(e)", 1);

    // equation tests:
    test("x+1=0", -1);
    test("x=1", 1);
    test("x-10=0", 10);

    test("3x+1=10", 3);
    test("x/3=1", 3);
    test("x=0", 0);
    test("2 * x + 3 = 0", -1.5);

//    test("x * x - 1", 0);
//    test("1/x", 0);
//    test("1/x", 0);
//    test("0*x", std::numeric_limits<double>::signaling_NaN());
}

int main(int argc, char *argv[])
{
    if(argc == 1 || (argc == 2 && strcmp(argv[1], "help") == 0))
    {
        std::cout << "It's a smart console calculator. For more examples see test method" << std::endl;
        return 0;
    }

    if(argc == 1 || (argc == 2 && strcmp(argv[1], "run_tests") == 0))
    {
        runTests();
        return 0;
    }

    std::string input;
    for (int i = 1; i < argc; ++i)
        input += argv[i];

    ScientificCalculator::printSolution(input);
    return 0;
}
